package com.square.megacalculator;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.icu.text.DecimalFormat;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText number1;
    EditText number2;
    Button Add_button;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // by ID we can use each component which id is assign in xml file
        number1=(EditText) findViewById(R.id.editText_first_no);
        number2=(EditText) findViewById(R.id.editText_second_no);
        Add_button=(Button) findViewById(R.id.add_button);
        result = (TextView) findViewById(R.id.textView_answer);

        // Add_button add clicklistener
        Add_button.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            public void onClick(View v) {

                // file size
                double num1 = Double.parseDouble((number1.getText()).toString());
                // speed
                double num2 = Double.parseDouble((number2.getText()).toString());

                // 1gb = 1024 mb
                double mb = 1024 * num1; // size in megabyte

                // downloading per sec
                double sec = 1 * mb / num2; // downloading per sec

                // 60 sec = 1 min
                double min = 1 * sec / 60; // downloading per min

                // 60 min = 1 h
                double r = 1 * min / 60; // downloading per hour

                // format decimal point
                DecimalFormat number = new DecimalFormat("#,##0.00");
                result.setText(number.format(r)+" Min");
                //result.setText(Double.toString(r));

            }
        });

    }

    // menu on action bar
    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,m);
        return true;
    }

    /*
        IF YOU GET ANY ERROR OF ACTIVITY
        JUST FUCK YOUR SELF
     */

    // on menu option
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.help:

                // method 1
                startActivity(new Intent(getApplicationContext(), HelpMenu.class));

                break;

            case R.id.about:

                // method 2
                Intent intent = new Intent(MainActivity.this, AboutMenu.class);
                startActivity(intent);

                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
