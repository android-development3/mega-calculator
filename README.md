# Mega Calculator 

Enter your file size and download speed and it will calculate the **ETA**

I was learning android development. For some time I'll have to download movies and in my country internet speed is not so stable. So I needed a simple calculation app for calculate download time. So I've created this. 

### Screenshot 

![screen](app/src/main/res/drawable/mega_calc.png)


### Usage

 * File size must be in GB or in Decimal Format 
   ```
   1GB = 1
   1200 MB = 1.2
   ```
 * Enter download speed as Decimal Format
   ```
   1MBps = 1 
   500/700KBps = 0.5/0.7
   ```
 * The output result shows in X.xx(**h.min**) Min
   ```
   Example 1 Hour = 1.0 Min
   Example 30 Min = 0.30 Min
   ```



### Library 

 * Minimum SDK - 24 (Nougat 7.0)
 * Library **sdp** used here! 



### Download APK
[![download_image](app/src/main/res/drawable/download_image.png)](https://gitlab.com/android-development3/mega-calculator/-/raw/master/app/release/beta_0.1.9.apk)



### Bug Report and Development Process 

 * Bug report [here](https://gitlab.com/android-development3/mega-calculator/-/issues)
 * If you are looking for change log then go to **CHANGELOG** section
 * Release Versions can be found [here](https://gitlab.com/android-development3/mega-calculator/-/releases)
 * If you wanna gimme a suggestion then please add it in [here](https://gitlab.com/android-development3/mega-calculator/-/issues)
 * OR 
 * You can fork and update it on your own then just get a pull request. 

<br>

Development, <br>
**Farhan Sadik** <br>
*Square Development Group*

